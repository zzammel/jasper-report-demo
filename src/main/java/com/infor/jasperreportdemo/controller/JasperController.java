package com.infor.jasperreportdemo.controller;

import com.infor.jasperreportdemo.entity.Employee;
import com.infor.jasperreportdemo.service.EmployeeService;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import javax.annotation.Resource;

@RestController
@RequestMapping( value = "/" )
public class JasperController {

    @Resource EmployeeService employeeService;

    @RequestMapping( value = "runEmployeeTabular", method = RequestMethod.GET,
                     produces = MediaType.APPLICATION_JSON_VALUE )
    public String runEmployeeTabular() {

        return employeeService.runEmployeeTabular();
    }
}
