package com.infor.jasperreportdemo.entity;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@NamedQueries( {
        @NamedQuery( name = "Employee.findAllEmployee", query = "from Employee as e order by e.id asc" )
} )


@Entity
@Table( name = "EMPLOYEE" )
@GenericGenerator( name = "EmployeeSequenceGenerator", strategy = "sequence",
                   parameters = { @Parameter( name = "sequence_name", value = "EMPLOYEES_ID_SEQ" ) } )
public class Employee {

    public static final String FIRST_NAME = "firstName";
    public static final String LAST_NAME = "lastName";
    public static final String GENDER = "gender";
    public static final String COUNTRY = "country";
    public static final String CITY = "city";
    public static final String EMAIL = "email";
    public static final String COMPANY_NAME = "companyName";
    public static final String DEPARTMENT = "department";
    public static final String SALARY = "salary";
    public static final String CAR_MODEL = "carModel";
    public static final String BITCOIN_ADDRESS = "bitcoinAddress";


    private Long id;
    private String firstName;
    private String lastName;
    private String gender;
    private String country;
    private String city;
    private String email;
    private String companyName;
    private String department;
    private String salary;
    private String carModel;
    private String bitcoinAddress;


    @Id
    @GeneratedValue( generator = "EmployeeSequenceGenerator" )
    @Column( name = "ID", nullable = false, updatable = false )
    public Long getId() {

        return id;
    }

    public void setId( Long id ) {

        this.id = id;
    }

    @Column( name = "FIRST_NAME", length = 50 )
    public String getFirstName() {

        return firstName;
    }

    public void setFirstName( String firstName ) {

        this.firstName = firstName;
    }

    @Column( name = "LAST_NAME", length = 50 )
    public String getLastName() {

        return lastName;
    }

    public void setLastName( String lastName ) {

        this.lastName = lastName;
    }

    @Column( name = "GENDER", length = 50 )
    public String getGender() {

        return gender;
    }

    public void setGender( String gender ) {

        this.gender = gender;
    }

    @Column( name = "COUNTRY", length = 50 )
    public String getCountry() {

        return country;
    }

    public void setCountry( String country ) {

        this.country = country;
    }

    @Column( name = "CITY", length = 50 )
    public String getCity() {

        return city;
    }

    public void setCity( String city ) {

        this.city = city;
    }

    @Column( name = "EMAIL", length = 50 )
    public String getEmail() {

        return email;
    }

    public void setEmail( String email ) {

        this.email = email;
    }

    @Column( name = "COMPANY_NAME", length = 50 )
    public String getCompanyName() {

        return companyName;
    }

    public void setCompanyName( String companyName ) {

        this.companyName = companyName;
    }

    @Column( name = "DEPARTMENT", length = 50 )
    public String getDepartment() {

        return department;
    }

    public void setDepartment( String department ) {

        this.department = department;
    }

    @Column( name = "SALARY", length = 50 )
    public String getSalary() {

        return salary;
    }

    public void setSalary( String salary ) {

        this.salary = salary;
    }

    @Column( name = "CAR_MODEL", length = 50 )
    public String getCarModel() {

        return carModel;
    }

    public void setCarModel( String carModel ) {

        this.carModel = carModel;
    }

    @Column( name = "BITCOIN_ADDRESS", length = 50 )
    public String getBitcoinAddress() {

        return bitcoinAddress;
    }

    public void setBitcoinAddress( String bitcoinAddress ) {

        this.bitcoinAddress = bitcoinAddress;
    }
}