package com.infor.jasperreportdemo.jasper.impl;

import com.infor.jasperreportdemo.jasper.JasReport;

import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import java.io.ByteArrayInputStream;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TabularReport implements JasReport {

    private String reportTitle;
    private List<String> selectedFields;
    private List<Map<String, String>> reportData;
    private JasperPrint jasperPrint;

    public TabularReport( String reportTitle, List<String> selectedFields, List<Map<String, String>> reportData ) {

        this.reportTitle = reportTitle;
        this.selectedFields = selectedFields;
        this.reportData = reportData;
    }

    @Override public void buildReport() {

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append( "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" );
        stringBuilder.append(
                "<jasperReport xmlns=\"http://jasperreports.sourceforge.net/jasperreports\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd\" name=\"employees\" pageWidth=\"595\" pageHeight=\"842\" columnWidth=\"535\" leftMargin=\"20\" rightMargin=\"20\" topMargin=\"20\" bottomMargin=\"20\" uuid=\"ea9d9eab-78ea-4cb9-adce-7588ae6a483e\">\n" );

        stringBuilder.append( "<parameter name=\"REPORT_TITLE\" class=\"java.lang.String\"/>\n" );
        stringBuilder.append( this.getFields() );
        stringBuilder.append( this.getBackGround() );
        stringBuilder.append( this.getTitle() );
        stringBuilder.append( this.getPageHeader() );
        stringBuilder.append( this.getColumnHeader() );
        stringBuilder.append( this.getDetails() );
        stringBuilder.append( this.getColumnFooter() );
        stringBuilder.append( this.getPageFooter() );
        stringBuilder.append( this.getSummary() );
        stringBuilder.append( "</jasperReport>" );

        try {
            //   PrintWriter printWriter = new PrintWriter( "/Users/mzammel/test.jrxml" );
            //   printWriter.println( stringBuilder.toString() );
            //   printWriter.close();
            JasperReport jasperReport =
                    JasperCompileManager.compileReport( new ByteArrayInputStream( stringBuilder.toString()
                                                                                          .getBytes( StandardCharsets.UTF_8 ) ) );
            JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource( reportData );
            Map<String, Object> parameters = new HashMap<>();
//            reportTitle = "تقرير الموظفين";
//            reportTitle = "รายงานพนักงาน";
//            reportTitle = "员工报告";
            parameters.put( "REPORT_TITLE", reportTitle );
            jasperPrint = JasperFillManager.fillReport( jasperReport, parameters, dataSource );
            //TODO: to change the output path here!!
            JasperExportManager.exportReportToPdfFile( jasperPrint, "/Users/mzammel/employees.pdf" );
        }
        catch ( Exception ex ) {
            ex.printStackTrace();
        }


    }

    private String getDetails() {

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append( "<detail>\n" +
                              "        <band height=\"20\">\n" +
                              "            <line>\n" +
                              "                <reportElement positionType=\"FixRelativeToBottom\" x=\"0\" y=\"19\" width=\"555\" height=\"1\" uuid=\"305d3455-6f0a-49e7-ad2a-055974675fd4\"/>\n" +
                              "            </line>" );
        int x = 0;
        for ( String field : selectedFields ) {
            stringBuilder.append( "<textField isStretchWithOverflow=\"true\">\n" +
                                  "                <reportElement x=\"" + x +
                                  "\" y=\"0\" width=\"111\" height=\"20\" uuid=\"1324277f-1cf1-4cc2-a5f3-88ededc0ff6e\">\n" +
                                  "                    <property name=\"com.jaspersoft.studio.spreadsheet.connectionID\" value=\"40a9814d-ecb4-424f-aea8-cad246f112b5\"/>\n" +
                                  "                </reportElement>\n" +
                                  "                <textElement textAlignment=\"Center\">\n" +
                                  "                    <font size=\"14\"/>\n" +
                                  "                </textElement>\n" +
                                  "                <textFieldExpression><![CDATA[$F{" + field +
                                  "}]]></textFieldExpression>\n" +
                                  "            </textField>" );
            x += 111;
        }
        stringBuilder.append( "</band>" );
        stringBuilder.append( "</detail>" );

        return stringBuilder.toString();
    }

    private String getColumnHeader() {

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append( "<columnHeader>\n" +
                              "        <band height=\"21\">\n" +
                              "            <line>\n" +
                              "                <reportElement x=\"-20\" y=\"20\" width=\"595\" height=\"1\" forecolor=\"#666666\" uuid=\"ca533965-9162-4dc2-8a9e-2c99b0db8e3e\"/>\n" +
                              "            </line>" );
        int x = 0;
        for ( String field : selectedFields ) {
            stringBuilder.append( "<staticText>\n" +
                                  "                <reportElement mode=\"Opaque\" x=\"" + x +
                                  "\" y=\"0\" width=\"111\" height=\"20\" forecolor=\"#006699\" backcolor=\"#E6E6E6\" uuid=\"55735968-342c-4d29-8dc1-db2263a44d11\">\n" +
                                  "                    <property name=\"com.jaspersoft.studio.spreadsheet.connectionID\" value=\"40a9814d-ecb4-424f-aea8-cad246f112b5\"/>\n" +
                                  "                </reportElement>\n" +
                                  "                <textElement textAlignment=\"Center\">\n" +
                                  "                    <font size=\"14\" isBold=\"true\"/>\n" +
                                  "                </textElement>\n" +
                                  "                <text><![CDATA[" + field + "]]></text>\n" +
                                  "            </staticText>" );
            x += 111;
        }

        stringBuilder.append( "</band>" );
        stringBuilder.append( "</columnHeader>" );
        return stringBuilder.toString();
    }

    private String getColumnFooter() {

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append( "<columnFooter>\n" +
                              "        <band/>\n" +
                              "    </columnFooter>" );
        return stringBuilder.toString();
    }

    private String getPageHeader() {

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append( "<pageHeader>" );
        stringBuilder.append( "     <band height=\"13\"/>" );
        stringBuilder.append( "</pageHeader>" );
        return stringBuilder.toString();
    }

    private String getPageFooter() {

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append( "<pageFooter>\n" +
                              "        <band height=\"17\">\n" +
                              "            <textField>\n" +
                              "                <reportElement mode=\"Opaque\" x=\"0\" y=\"4\" width=\"515\" height=\"13\" backcolor=\"#E6E6E6\" uuid=\"c7f5dcbd-921c-4949-8f5c-3922c35efa2c\"/>\n" +
                              "                <textElement textAlignment=\"Right\"/>\n" +
                              "                <textFieldExpression><![CDATA[\"Page \"+$V{PAGE_NUMBER}+\" of\"]]></textFieldExpression>\n" +
                              "            </textField>\n" +
                              "            <textField evaluationTime=\"Report\">\n" +
                              "                <reportElement mode=\"Opaque\" x=\"515\" y=\"4\" width=\"40\" height=\"13\" backcolor=\"#E6E6E6\" uuid=\"0f776560-e86c-4c18-bb5a-76c0842abe06\"/>\n" +
                              "                <textFieldExpression><![CDATA[\" \" + $V{PAGE_NUMBER}]]></textFieldExpression>\n" +
                              "            </textField>\n" +
                              "            <textField pattern=\"EEEEE dd MMMMM yyyy\">\n" +
                              "                <reportElement x=\"0\" y=\"4\" width=\"100\" height=\"13\" uuid=\"4f042a9c-a347-4667-9795-ae1c1612e30f\"/>\n" +
                              "                <textFieldExpression><![CDATA[new java.util.Date()]]></textFieldExpression>\n" +
                              "            </textField>\n" +
                              "        </band>\n" +
                              "    </pageFooter>" );
        return stringBuilder.toString();
    }

    private String getSummary() {

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append( "<summary>\n" +
                              "        <band/>\n" +
                              "    </summary>" );
        return stringBuilder.toString();
    }

    private String getFields() {

        StringBuilder stringBuilder = new StringBuilder();
        for ( String field : selectedFields ) {
            stringBuilder.append( "<field name=\"" + field + "\" class=\"java.lang.String\"/>" );
        }
        return stringBuilder.toString();
    }

    private String getBackGround() {

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append( " <background>" );
        stringBuilder.append( "     <band/>" );
        stringBuilder.append( " </background>" );
        return stringBuilder.toString();
    }

    private String getTitle() {

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append( "<title>\n" +
                              "\t\t<band height=\"72\">\n" +
                              "\t\t\t<frame>\n" +
                              "\t\t\t\t<reportElement mode=\"Opaque\" x=\"-20\" y=\"-20\" width=\"595\" height=\"92\" backcolor=\"#006699\" uuid=\"067e5760-2a3c-4197-92e5-afbec0f9ce47\"/>\n" +
                              "\t\t\t\t<textField>\n" +
                              "\t\t\t\t\t<reportElement x=\"10\" y=\"10\" width=\"370\" forecolor=\"#FFFFFF\" height=\"70\" uuid=\"471195d3-db23-413f-b3be-d0717401964c\"/>\n" +
                              "<textElement>\n" +
                              "                        <font fontName=\"Great Vibes\" size=\"34\" isBold=\"true\"/>\n" +
                              "                    </textElement>" +
                              "\t\t\t\t\t<textFieldExpression><![CDATA[$P{REPORT_TITLE}]]></textFieldExpression>\n" +
                              "\t\t\t\t</textField>\n" +
                              "\t\t\t\t<staticText>\n" +
                              "\t\t\t\t\t<reportElement x=\"395\" y=\"43\" width=\"180\" height=\"20\" forecolor=\"#FFFFFF\" uuid=\"d542e825-fb57-41bd-b967-ba4a22515efe\"/>\n" +
                              "\t\t\t\t\t<textElement>\n" +
                              "\t\t\t\t\t\t<font size=\"14\" isBold=\"false\"/>\n" +
                              "\t\t\t\t\t</textElement>\n" +
                              "\t\t\t\t\t<text><![CDATA[Jasper Demo]]></text>\n" +
                              "\t\t\t\t</staticText>\n" +
                              "\t\t\t</frame>\n" +
                              "\t\t</band>\n" +
                              "\t</title>" );

        return stringBuilder.toString();
    }


}
