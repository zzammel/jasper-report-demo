package com.infor.jasperreportdemo.repository;

import com.infor.jasperreportdemo.entity.Employee;

import java.util.List;
import java.util.Map;

public interface EmployeeDao {

    List<Map<String, String>> findEmployeeWithQueryParams( List<String> selectedFields );
}
