package com.infor.jasperreportdemo.repository.impl;

import com.infor.jasperreportdemo.entity.Employee;
import com.infor.jasperreportdemo.repository.EmployeeDao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


@Repository
public class EmployeeRepositoryImpl implements EmployeeDao {


    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private SessionFactory sessionFactory;

    @Override public List<Map<String, String>> findEmployeeWithQueryParams( List<String> selectedFields ) {

        Session session = sessionFactory.openSession();
        try {
            session.beginTransaction();
            String query_fields = "";
            for ( String field : selectedFields ) {
                if ( query_fields.isEmpty() ) {
                    query_fields += "e." + field;
                }
                else {
                    query_fields += ",e." + field;
                }
            }
            String select = "select " + query_fields + " from Employee e";
            Query query = session.createQuery( select );
            List<Object[]> results = query.list();
            List<Map<String, String>> employeeList = new ArrayList<>();
            for ( Object[] objects : results ) {
                Map<String, String> row = new HashMap<>();
                for ( int i = 0; i < selectedFields.size(); i++ ) {
                    row.put( selectedFields.get( i ), (String) objects[i] );
                }
                employeeList.add( row );
            }
            session.close();
            return employeeList;
        }
        catch ( Exception ex ) {
            ex.printStackTrace();
        }

        session.close();
        return null;

    }


}
