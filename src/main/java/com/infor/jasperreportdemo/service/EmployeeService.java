package com.infor.jasperreportdemo.service;

import com.infor.jasperreportdemo.entity.Employee;

import java.util.List;

public interface EmployeeService {

    String runEmployeeTabular();
}
