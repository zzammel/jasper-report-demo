package com.infor.jasperreportdemo.service.impl;

import com.infor.jasperreportdemo.entity.Employee;
import com.infor.jasperreportdemo.jasper.impl.TabularReport;
import com.infor.jasperreportdemo.repository.EmployeeDao;
import com.infor.jasperreportdemo.service.EmployeeService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired EmployeeDao employeeDao;


    @Override public String runEmployeeTabular() {

        //Prepare the selectedFields list to build the query

        List<String> selectedFields = new ArrayList<>();
        selectedFields.add( Employee.FIRST_NAME );
        selectedFields.add( Employee.LAST_NAME );
        selectedFields.add( Employee.GENDER );
        selectedFields.add( Employee.COMPANY_NAME );
        selectedFields.add( Employee.DEPARTMENT );
        selectedFields.add( Employee.SALARY );
      //  selectedFields.add( Employee.COUNTRY );

        //Prepare the reportData
        List<Map<String, String>> reportData = employeeDao.findEmployeeWithQueryParams( selectedFields );

        //Create the report.
        TabularReport tabularReport=new TabularReport( "Employees Report",selectedFields,reportData );
        tabularReport.buildReport();

        return "Success";
    }
}
